                                                                                                             //Background
var horizon;
var obstacleSpeed;
var score;
var obstacles = [];
var dino;
function setup() {
    
    createCanvas(600, 200);
    textAlign(CENTER);
    horizon = height - 40;
    score = 0;
    obstacleSpeed = 6;
    var size = 20;
    dino = new TRex(size * 2, height - horizon, size);
    textSize(20);
}

function draw() {
    background(51);
    drawHUD();
    handleLevel(frameCount);
    dino.update(horizon);
    handleObstacles();
}

//draws horizon & score
function drawHUD() {

    /* draw horizon */
    stroke(255);
    strokeWeight(2);
    line(0, horizon, width, horizon);

    /* draw score */
    noStroke();
    text("Score: " + score, width / 2, 30);

    /* draw T-Rex */
    dino.draw();
}

//updates, draws, and cleans out the obstacles
function handleObstacles() {

    for (var i = obstacles.length - 1; i >= 0; i--) {
        obstacles[i].update(obstacleSpeed);
        obstacles[i].draw();
        if (obstacles[i].hits(dino)) // if there's a collision
            endGame();
        if (!obstacles[i].onScreen) // if it's no longer showing
            obstacles.splice(i, 1); // delete from array
    }
}

//speeds game up, pushes new obstacles, & handles score
function handleLevel(n) {
    if (n % 30 === 0) { // every 0.5 seconds
        var n = noise(n); // noisey
        if (n > 0.5)
            newObstacle(n); // push new obstacle
        if (n % 120 === 0) // every 2 seconds
            obstacleSpeed *= 1.05; // speed up
    }
    score++;
}
//pushes random obstacle
function newObstacle(n) {
    var col = color(random(255), random(255), random(255));
    var size = random(30) + 20;
    var obs = new Obstacle(width + size, size, horizon, col);
    obstacles.push(obs);
}
function keyPressed() {
    if ((keyCode === UP_ARROW || keyCode === 32) && dino.onGround) // jump if possible
        dino.jump();
}
function endGame() {
    noLoop();
    noStroke();
    textSize(40);
    text("GAME OVER", width / 2, height / 2);
    textSize(20);
    text("Press f5 to restart", width / 2, height / 2 + 20);
}
                                                                                                             //Objects
function TRex(x, y, radius) {
    this.x = x;
    this.y = y;
    this.yVelocity = 0;
    this.speed = 1;
    this.onGround = true;
    this.radius = radius; // size of circle
}

//handle y values
TRex.prototype.update = function (platform) {

    var bottom = this.y + this.radius; // bottom pixel of circle
    var nextBottom = bottom + this.yVelocity; // calculate next frame's bottom
    if (bottom <= platform && nextBottom >= platform) { // next frame will be on platform
        this.yVelocity = 0; // reset velocity
        this.y = platform - this.radius; // don't go past platform
        this.onGround = true;
    } else if (platform - bottom > 1) { // nowhere near platform
        this.yVelocity += this.speed; // increase velocity
        this.onGround = false;
    }

    /* movement */
    this.y += this.yVelocity;
};

//make the object jump
TRex.prototype.jump = function () {
    this.yVelocity = -(this.radius * 0.7); // jump
};

TRex.prototype.draw = function () {

    fill('#999999');
    stroke(255);
    strokeWeight(2);
    ellipse(this.x, this.y, this.radius * 2);
};

                                                                                                             //Obstacles
function Obstacle(x, size, horizon, color) {                                                  
    this.x = x;
    this.y = horizon - size;
    this.size = size;                                           
    this.color = color;
    this.onScreen = true;
}

//handle x and onScreen values
Obstacle.prototype.update = function (speed) {

    /* check if offscreen */
    this.onScreen = (this.x > -this.size);

    /* movement */
    this.x -= speed;
};

Obstacle.prototype.draw = function () {
    fill(this.color);
    stroke(255);
    strokeWeight(2);
    rect(this.x, this.y, this.size, this.size);
};
//checks for collisions
Obstacle.prototype.hits = function (dino) {
    var halfSize = this.size / 2;
    var minimumDistance = halfSize + (dino.radius); // closest before collision

    /* find center coordinates */
    var xCenter = this.x + halfSize;
    var yCenter = this.y + halfSize;
    var distance = dist(xCenter, yCenter, dino.x, dino.y); // calculate distance from centers
    return (distance < minimumDistance); // return result
};
